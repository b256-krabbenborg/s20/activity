// console.log("Hello World!")

/*3. Create a variable number that will store the value of the number provided by the user via the prompt.*/

/*4. Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.*/

/*5. Create a condition that if the current value is less than or equal to 50, stop the loop.*/

/*6. Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.*/

/*7. Create another condition that if the current value is divisible by 5, print the number.*/

let number = parseInt(prompt("Please enter a number:"));

console.log("The number you provided is", number + ".");

for (let i = number; i > 0; i--) {

  if (i <= 50) {
    console.log("The current value is at 50. Terminating the loop.");
    break;
  }

  if (i % 10 === 0) {
    console.log("The number is divisible by 10. Skipping the number.");
    continue;
  }

  if (i % 5 === 0) {
    console.log(i + ".");
  }

}


/*8. Create a variable that will contain the string supercalifragilisticexpialidocious.*/

/*9. Create another variable that will store the consonants from the string.*/

/*10. Create another for Loop that will iterate through the individual letters of the string based on its length.*/

/*11. Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.*/

/*12. Create an else statement that will add the letter to the second variable.*/


let longWord = "supercalifragilisticexpialidocious";
let consonants = "";
for (let i = 0; i < longWord.length; i++) {
  if (/[aeiou]/i.test(longWord[i])) {
    continue;
  }
  consonants += longWord[i];
}

console.log(longWord);
console.log(consonants);